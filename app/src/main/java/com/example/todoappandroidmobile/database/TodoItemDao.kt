package com.example.todoappandroidmobile.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.todoappandroidmobile.database.model.TodoItem
import kotlinx.coroutines.flow.Flow

// Implement TodoItemDao using Room. It should contain following methods:
// - getAll() : Flow<List<TodoItem>>
// - add(item: TodoItem)
// - delete(item: TodoItem)
// - update(item: TodoItem)
@Dao
interface TodoItemDao {
    @Query("SELECT * FROM todo_item")
    fun getAll(): Flow<List<TodoItem>>

    @Insert
    suspend fun add(item: TodoItem)

    @Delete
    suspend fun delete(item: TodoItem)

    @Update
    suspend fun update(item: TodoItem)
}
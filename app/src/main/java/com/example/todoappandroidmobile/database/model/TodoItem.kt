package com.example.todoappandroidmobile.database.model

// write TodoItem data class that should contain todo item name, it's status (completed or not) and PrimaryKey Id
// text should be stored in Room database with name "item_text" and should be not null
// isCompleted should be stored in Room database with name "is_completed" and should be not null
@androidx.room.Entity(tableName = "todo_item")
data class TodoItem(
    @androidx.room.ColumnInfo(name = "item_text")
    val text: String,
    @androidx.room.ColumnInfo(name = "is_completed")
    val isCompleted: Boolean = false,
    @androidx.room.PrimaryKey(autoGenerate = true)
    val id: Long = 0
)
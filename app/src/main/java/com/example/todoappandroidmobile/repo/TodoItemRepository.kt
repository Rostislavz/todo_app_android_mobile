package com.example.todoappandroidmobile.repo

import com.example.todoappandroidmobile.database.model.TodoItem
import kotlinx.coroutines.flow.Flow

/* write TodoItemRepository interface that should contain :
   - property to get TodoItem items as Flow
   - method to add TodoItem item
   - method to delete TodoItem item
   - method to complete TodoItem item
 */
interface TodoItemRepository {
    val items: Flow<List<TodoItem>>
    suspend fun add(item: TodoItem)
    suspend fun delete(item: TodoItem)
    suspend fun complete(item: TodoItem)
}
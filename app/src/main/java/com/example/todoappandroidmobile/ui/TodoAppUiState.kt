package com.example.todoappandroidmobile.ui

import com.example.todoappandroidmobile.database.model.TodoItem

/* write TodoAppUiState data class to represent current list of todo items and user input text
 */
data class TodoAppUiState(val items: List<TodoItem>, val text: String)
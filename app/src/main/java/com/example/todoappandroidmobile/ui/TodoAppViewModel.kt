package com.example.todoappandroidmobile.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.CreationExtras
import com.example.todoappandroidmobile.TodoAppApplication
import com.example.todoappandroidmobile.database.TodoAppDatabase
import com.example.todoappandroidmobile.repo.TodoItemRepository
import com.example.todoappandroidmobile.repo.TodoItemRepositoryInMemory
import com.example.todoappandroidmobile.database.model.TodoItem
import com.example.todoappandroidmobile.repo.TodoItemRepositoryDatabase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch

/* write TodoAppViewModel that will take TodoItemRepository as a constructor parameter and implement ViewModel*/
@Suppress("UNCHECKED_CAST")
class TodoAppViewModel(private val repository: TodoItemRepository) : ViewModel() {
    // write inputText field as MutableStateFlow<String> that will be updated when text input field is changed and starts with empty string
    private val inputText = MutableStateFlow("")

    /* write hidden _uiState : SharedStateFlow<TodoAppUiState>  property with empty TodoAppUiState as initial value */
    private val _uiState = MutableStateFlow(TodoAppUiState(emptyList(), ""))
    /* write public uiState that will be initialized with _uiState */
    val uiState: StateFlow<TodoAppUiState> = _uiState

    /*
       write init block where items from repository will be collected and combined with inputText field.
       viewModelScope should be used to launch collection.
       items emitted from repository should be copied to new list
    */
    init {
        viewModelScope.launch {
            repository.items.combine(inputText) { items, text ->
                TodoAppUiState(items.toList(), text)
            }.collect {
                _uiState.value = it
            }
        }
    }

    // write method updateText that will update inputText field using viewModelScope
    fun updateText(text: String) {
        inputText.value = text
    }

    //write method addItem that will take inputText field value, create TodoItem and add to TodoItemRepository using viewModelScope
    fun addItem() {
        viewModelScope.launch {
            val text = inputText.value
            if (text.isNotBlank()) {
                repository.add(TodoItem(text, false))
                inputText.value = ""
            }
        }
    }
    // write method deleteItem that will take TodoItem as an input will delete TodoItem from TodoItemRepository
    fun deleteItem(item: TodoItem) {
        viewModelScope.launch {
            repository.delete(item)
        }
    }
    // write method completeItem that will take TodoItem as an input and will complete TodoItem from TodoItemRepository
    fun completeItem(item: TodoItem) {
        viewModelScope.launch {
            repository.complete(item)
        }
    }

    // Define ViewModel factory object in a companion object that is instance of ViewModelProvide.Factory and takes database as a parameter
    companion object {
        val Factory : (TodoAppDatabase) -> ViewModelProvider.Factory = {
            database: TodoAppDatabase ->
            object : ViewModelProvider.Factory {
                override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T {
                    return TodoAppViewModel(TodoItemRepositoryDatabase(database.todoItemDao())) as T
                }
            }
        }
    }
}
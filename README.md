# Todo App Android Mobile

### Project Overview and User Outcomes

#### Todo App Android Mobile: Purpose and Functionality

**What This Project Is For:**

-   The Todo App Android Mobile is designed as a productivity tool to help users manage their tasks and to-dos efficiently. It's an Android application that offers a user-friendly interface for adding, organizing, and tracking tasks.
-   The app aims to simplify task management with features like categorizing tasks, setting deadlines, and receiving notifications. This makes it ideal for anyone looking to improve their time management and organization skills.
-   Beyond its practical use, this project serves as a valuable resource for developers looking to understand Android app development. It offers insights into app design, functionality, and the integration of various Android development tools.

#### User Outcomes After Installation and Setup

**What Users Will Do and Achieve:**

-   **For Developers:**

    -   Developers who follow the installation instructions will have a working instance of the Todo App Android Mobile project on their system.
    -   This process provides practical experience in setting up and running an Android development environment.
    -   Developers will gain hands-on experience with the project, offering valuable insights into Android app development, including coding practices, app design, and functionality implementation.
    -   This experience is crucial for those looking to enhance their skills in mobile application development or to contribute to similar open-source projects.

### Tool Installation Guide

#### For All Users (Windows, Mac, Linux)

1.  **Android Studio:**

    -   Download from [official website](https://developer.android.com/studio).
    -   **Installation:**
        -   **Windows:** Run the .exe file and follow the prompts.
        -   **Mac:** Open the .dmg file and drag Android Studio to the Applications folder.
        -   **Linux:** Unpack the .tar.gz file to an appropriate location and run the studio.sh script.
    -   **Validation:** Open Android Studio. If it launches to the welcome screen, it's correctly installed.
2.  **Git:**

    -   Download from [Git's official website](https://git-scm.com/downloads).
    -   **Installation:**
        -   Follow on-screen prompts for your specific OS.
    -   **Validation:** In terminal (Linux/Mac) or command prompt (Windows), run `git --version`. If it shows a version number, Git is installed.
3.  **JDK (Java Development Kit):**

    -   Download from [Oracle's website](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html).
    -   **Installation:**
        -   Follow on-screen prompts for your specific OS.
    -   **Validation:** In terminal or command prompt, run `java -version`. If it shows a version number, JDK is installed.

### Project (from Repo) Installation Guide

#### For All Users (Windows, Mac, Linux)

1.  **Clone the Repository:**

    -   Open terminal or command prompt.
    -   Run `git clone https://gitlab.com/j2ko/todo_app_android_mobile.git`.
    -   Navigate to the cloned directory.
2.  **Open and Run the Project in Android Studio:**
    -   Open Android Studio.
    -   Choose 'Open an Existing Project' and navigate to your cloned repository folder.
    -   Allow Android Studio to build the project (this may take some time).

3.  **Project Installation Validation:**

    -   Once the build is complete, run the app on an emulator or a physical device.
    -   If the app launches and operates without errors, the project is correctly installed and running.